
# Tutorial ACP-AI-OR-20: Learning Car Configuration Preferences

## Prerequisites

The python3-sklearn (scikit python3) must be installed. 

## Data files
* **1-year sales history of satisfiable car configurations**: config_medium_filtered.txt.xz (8253) / config_big_filtered.txt.xz (8338) (car configurations incompatible with the constraints have been removed and repeated configurations have been duplicated)
* **Domains**: domain_medium.pkl / domain_big.pkl (pickle ordered domain dictionary data structures)
* **Constraints**: medium_domainsorted.xml / big_domainsorted.xml.xz (domain values have been sorted in increasing order compared to the original files)
* **Indices of the 10-fold cross-validation for each sale history**: index_csv_medium.txt / index_csv_big.txt (duplicated configurations are kept in the same fold)

Original datasets come from [here](https://www.irit.fr/~Helene.Fargier/BR4CP/benches.html).

## Lambda parameters
The lambda parameter selected using the Stability Approach to Regularization Selection (StARS) algorithm [[Liu et al, NIPS 2010]](http://papers.nips.cc/paper/3966-stability-approach-to-regularization-selection-stars-for-high-dimensional-graphical-models) for the different folds of the cross-validation are given in the corresponding files:
* lambda_medium_l1.txt
* lambda_medium_l1_l2.txt
* lambda_big_l1.txt
* lambda_big_l1_l2.txt

## Main script renault.py
The parameters are:
1. penalty norm [0-2] (L1/L2/L1_L2 norms)
2. lambda value [0-infty] (unused if last parameter set to 1)
3. validation fold [0-9]
4. instance name [medium|big] 
5. minimum arity of learned cost functions [1|2] (default value is 1)
6. maximum arity of learned cost functions [1|2] (default value is 2)
7. combining learned preferences with known constraints [0|1] (default value is 1)
8. comparison with predictions from an oracle method knowing the test set [0|1] (default value is 0)
9. initial random seed number [integer] (default value is 1)
10. find lambda value automatically using the StARS algorithm [0|1] (default value is 0)

## Experiments

1. Count the number of solutions on a `medium` car product description by exploiting tree decomposition [[Favier et al, CP2009]](http://miat.inrae.fr/degivry/Favier09a.pdf).
```
cd renault
unxz *.xml.xz
toulbar2 medium_domainsorted.xml -ub=1 -a -O=-3 -B=1 -hbfs: -nopre
```

2. Learn the user preferences using L1 norm with pre-recorded lambda found by the StARS algorithm and combine them to the previous car product description in order to simulate an on-line car configuration prediction tool, using the last test fold of a 10-fold cross validation protocol.
```
pip3 install sklearn
python3 renault.py 0 29.151 9 medium
```

3. Example for running the code on the `medium` dataset with an L1 penalty using pre-recorded lambda for 10-fold cross validation and 10 repetitions:
```
/bin/bash
filename=lambda_medium_l1.txt
declare -a myArray
myArray=(`cat "$filename"`)
for ((fold=0; fold<10 ; i++))
do
    for ((j=0; j<10 ; j++))
    do
	    python3 renault.py 0 ${myArray[$fold]} ${fold} medium 1 2 1 0 ${j}
    done
done
```

![Fig.2a [Brouard et al, CP2020]](http://genoweb.toulouse.inra.fr/~degivry/evalgm/medium.png)

4. Count the number of solutions on a `big` car product description.
```
toulbar2 big_domainsorted.xml -ub=1 -a -O=-3 -B=1 -hbfs: -nopre
```

5. Learn the user preferences using L1 norm with pre-recorded lambda and combine them to the previous car product description in order to simulate an on-line car configuration prediction tool, using the last test fold of a 10-fold cross validation protocol.
```
python3 renault.py 0 0.231 9 big
```

![Fig.2b [Brouard et al, CP2020]](http://genoweb.toulouse.inra.fr/~degivry/evalgm/big.png)

