
import sys
import CFN

f = open(sys.argv[1], 'r').readlines()

# number of runways
K = int(sys.argv[2])  

tokens = []
for l in f:
    tokens += l.split()

pos = 0

def token():
   global pos, tokens
   if (pos == len(tokens)):
      return None
   s = tokens[pos]
   pos += 1
   return int(float(s)) 

# number of airplanes
N = token()
token() # skip freeze time

LT = []
PC = []
ST = []

for i in range(N):
   token()  # skip appearance time
# Times per plane: {earliest landing time, target landing time, latest landing time}
   LT.append([token(), token(), token()])

# Penalty cost per unit of time per plane:
# [for landing before target, after target]
   PC.append([token(), token()])

# Separation time required after i lands before j can land
   ST.append([token() for j in range(N)])

top = 99999

Problem = CFN.CFN(top)
CFN.tb2.option.showSolutions = 3
#CFN.tb2.option.allSolutions = 1000
CFN.tb2.option.verbose = -1
CFN.tb2.check()

# airplane i lands at time t_i
for i in range(N):
    Problem.AddVariable('t' + str(i), range(LT[i][0],LT[i][2]+1))

# airplane i lands on runway r_i
for i in range(N):
    Problem.AddVariable('r' + str(i), range(K))

# penalty cost for landing before or after target landing time
for i in range(N):
    Problem.AddFunction([i], [PC[i][0] * abs(t - LT[i][1]) for t in range(LT[i][0], LT[i][2]+1)])

# non overlap constraint between every pair of airplanes
for i in range(N):
    for j in range(i+1,N):
        Problem.AddFunction([N+i, N+j, i, j], [(0 if (r_i != r_j or t_i + ST[i][j] <= t_j or t_j + ST[j][i] <= t_i) else top) for r_i in range(K) for r_j in range(K) for t_i in range(LT[i][0], LT[i][2]+1) for t_j in range(LT[j][0], LT[j][2]+1)])            

#Problem.Dump(sys.argv[1].replace('.txt','_' + str(K) + '.cfn'))
Problem.NoPreprocessing()
res=Problem.Solve() # returns (solution vector, optimum, number of solutions found)

print("Optimum:", res[1], "in", Problem.GetNbBacktracks(), "backtracks and", Problem.GetNbNodes(), "nodes.")
