# Tutorial ACP-AI-OR-20: Airplane Landing Problem

1. Open a terminal and visualize the data files using gnuplot.
```
cd airland
gnuplot -p -c airland.plot airland1.txt 
```

2. Run Visual Studio Code. Open folder `airland`. Open `airland.py`. Modify the code to take into account a different cost for landing earlier (`PC[i][0]`) or later (`PC[i][1]`).

3. Solve `airland0.txt` and observe the landing times.
```
python3 airland.py airland0.txt
```

4. Change the domain values to get time relative to target landing time. Solve again and verify the objective value.

5. Find the optimum of `airland1.txt`. How many optimal solutions are there? (Hints: see option allSolutions and top)
```
python3 airland.py airland1.txt
```

6. Try to solve a larger problem `airland5.txt`. Improve performances by using the Virtual Arc Consistency algorithm (Hint: add option vac=1 in the CFN constructor) 
```
python3 airland.py airland5.txt
```

7. Notice that two airplane time windows may not overlap (e.g. `airland6.txt`). In order to avoid creating useless (universal) constraints, add a condition to avoid them.

8. Open `airland2.py` for the multi-runways version. Solve `airland0.txt` with 1 or 3 runways. Compare with the previous version.
```
time python3 airland.py airland0.txt 1
time python3 airland2.py airland0.txt 1
time python3 airland2.py airland0.txt 3
```

9. Improve model `airland2.py` performances by replacing 4-arity cost functions by binary cost functions (Hint: merge time and runway domains into a single domain for each airplane).
```
time python3 airland.py airland0.txt 1
time python3 airland2.py airland1.txt 2
time python3 airland2.py airland1.txt 3
time python3 airland2.py airland5.txt 2
```

10. Add partial symmetry breaking constraints on runway selection (Hints: first airplane is assigned to runway 0, second airplane to 0 or 1, third to 0, 1, or 2, etc.)

11. Compare your results with an integer linear programming solver SCIP.
```
python3 airland2lp.py airland1.txt 1 > airland1.lp
scip -c 'read airland1.lp opt quit'
python3 airland2lp.py airland5.txt 1 > airland5.lp
scip -c 'read airland5.lp opt quit'
python3 airland2lp.py airland5.txt 2 > airland5_2.lp
scip -c 'read airland5_2.lp opt quit'
```

12. Compare with the MiniZincIDE constraint programming framework (Hints: download and install `or-tools FlatZinc binary` for Ubuntu18.04LTS uing `Add new solver` in MiniZincIDE). Try `airland2.mzn` with different number of runways.
```
awk -f ./airland2dzn.awk airland1.txt > airland1.dzn
minizinc airland.mzn airland1.dzn
awk -f ./airland2dzn.awk airland5.txt > airland5.dzn
minizinc airland.mzn airland5.dzn
minizinc airland2.mzn airland5.dzn
```

You can find mono-runway instances in compressed CFN format [here](http://genoweb.toulouse.inra.fr/~degivry/evalgm/CFN/AirLanding). More data files and models are available [here](https://forgemia.inra.fr/thomas.schiex/cost-function-library/-/tree/master/crafted/airland).

