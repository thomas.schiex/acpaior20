import numpy
import os
import pytoulbar2 as tb2

class CFN:
    def __init__(self, twoSolutions):
        tb2.init()
        tb2.option.hbfs = True
        tb2.option.vac = False
        tb2.option.verbose = -1
        tb2.option.showSolutions = False
        tb2.option.backtrackLimit = 50000
        tb2.option.weightedTightness = False
        tb2.option.decimalPoint = 6
        if (twoSolutions) :
            tb2.option.allSolutions = 2
        self.Variables = {}
        self.VariableIndices = {}
        self.Scopes = []
        self.VariableNames = []
        self.CFN = tb2.Solver()

    def __del__(self):
        del self.Scopes
        del self.Variables
        del self.VariableIndices
        del self.VariableNames

    def AddVariable(self, name, values):
        if name in self.Variables:
            raise Error(name+" already defined")
        cardinality = len(values)
        self.Variables[name] = values
        vIdx = self.CFN.wcsp.makeEnumeratedVariable(name, 0, len(values)-1)
        self.VariableIndices[name] = vIdx
        for vn in values:
            self.CFN.wcsp.addValueName(vIdx, vn);
        self.VariableNames.append(name)

    def AddFunction(self, scope, costs):
        sscope = set(scope)
        if len(scope) != len(sscope):
            raise("Error: duplicate variable in scope")
        arity = len(scope)
        for i, v in enumerate(scope):
            if isinstance(v, str):
                v = VariableIndices[v]
            if (v < 0 or v >= len(self.VariableNames)):
                raise("Error: out of range variable index") 
            scope[i] = v
        if (len(scope) == 1):
            self.CFN.wcsp.postUnaryConstraint(scope[0], costs)
        elif (len(scope) == 2):
            self.CFN.wcsp.postBinaryConstraint(scope[0], scope[1], costs)
        else:
            raise NameError('Higher than 2 arity functions not implemented yet in Python layer.')
        self.Scopes.append(sscope)
        return

    def UpdateUB(self, cost):
        icost = self.CFN.wcsp.DoubletoCost(cost)
        self.CFN.wcsp.updateUb(icost)

    def Solve(self):
        self.CFN.wcsp.sortConstraints()
        solved = self.CFN.solve()
        if (solved):
            return self.CFN.solution(),self.CFN.wcsp.getDPrimalBound(),len(self.CFN.solutions())
        else:
            return None
