#!/usr/bin/env python3
import lzma
import numpy as np
import math,time,sys,os
from PEMRF import *
import pandas as pd
from colorama import Fore, Style
import pickle

# computes a "best" solution of the CFN from exact hints. We try a
# heuristic ub and if no solution is found relax it.
def find_best_sol0(CFNdata,  hints):
    tb2_time = 0
    # we assign the hints and ask for a less than zero cost solution
    cfn = toCFN(*CFNdata, assign = hints)
    cfn.UpdateUB(1e-6)
    ctime = time.process_time()            
    sol = cfn.Solve()
    tb2_time += time.process_time()-ctime
    del cfn
    if (not sol):
        cfn = toCFN(*CFNdata, assign = hint)
        ctime = time.process_time()            
        sol = cfn.Solve()
        tb2_time += time.process_time()-ctime
        del cfn
    return sol, tb2_time

# computes a best solution of the CFN knowing only the hints as images
def find_best_sol12(CFNdata, fuzz_hints):
    tb2_time = 0
    # we add the unary cost functions that represent the confidence
    # scores of LeNet and use the heurisric ub
    cfn = toCFN(*CFNdata, weight = fuzz_hints,  btLimit = btlimit)
    cfn.UpdateUB(1e-6)
    ctime = time.process_time()                
    sol = cfn.Solve()
    tb2_time += time.process_time()-ctime    
    del cfn
    # else we relax our bound
    if (not sol):
        cfn = toCFN(*CFNdata, weight = fuzz_hints, btLimit = btlimit)
        ctime = time.process_time()            
        sol = cfn.Solve()
        tb2_time += time.process_time()-ctime
        del cfn
    return sol, tb2_time

def MNIST_transform(sample, sample_idx):
    lw = [[] for i in sample]
    for idx,c in enumerate(sample):
        if (c != '0'):
            img_idx = hash(c+str(idx)+str(sample_idx)) % logits_len[int(c)]
            lwi = list(map(lambda x: max(0,x), logits[int(c)][img_idx][1:]))
            minlwi = min(lwi)
            lw[idx] = list(map(lambda x: (x - minlwi),lwi))
    return lw

# decodes a grid (hint or solution) assuming it needs to be Lenet decoded
def MNIST_decode(sample, sample_idx):
    dec = []
    for idx,c in enumerate(sample):
        if (c != '0'):
            img_idx = hash(c+str(idx)+str(sample_idx)) % logits_len[int(c)]
            dec.append(logits[int(c)][img_idx][1:].argmin()+1)
        else:
            dec.append(0)
    return dec

# print the true solution with hints, its LeNet decoded variant (if given, mode
# 1/2) and the predicted solution (if given/found)
def pgrid(lt,lh,lp=None,ld=None):
    print()
    print("   S O L U T I O N            ",end='')
    if (ld): print("  D E C O D E D             ",end='')
    if (lp): print("P R E D I C T E D",end='')
    print('\n')
    for i in range(9):
        for j in range(3):
            print(" ".join([Fore.WHITE+str(a+1) if a==b else Style.RESET_ALL+str(a+1) for a,b in zip(lt[i*9+j*3:i*9+j*3+3],lh[i*9+j*3:i*9+j*3+3])]),end='   ')
        print(end='    ')
        if (lp):
                for j in range(3):
                    print(" ".join([Fore.WHITE+str(b+1) if c==b else Fore.GREEN+str(min(9,a+1)) if a==b else Fore.RED+str(min(9,a+1)) for a,b,c in zip(lp[i*9+j*3:i*9+j*3+3],lt[i*9+j*3:i*9+j*3+3],lh[i*9+j*3:i*9+j*3+3])]),end='   ')                    
        print()
        if (i%3 == 2): print()
    print(Style.RESET_ALL)

    
# Main section

if (len(sys.argv) not in [3]):
    print("Bad number of arguments!")
    print("training sample size [1000-180000] test set [satnet/rrn-??] ")
    exit()

mode = 0
norm_type = "l1"
training_size= int(sys.argv[1])
num_sample = 1000
A_matrix_fn = os.path.join("train-sufficient-statistics",("A_"+str(training_size))+".xz")
test_set = sys.argv[2]+".csv.xz"

test_CSV = pd.read_csv(test_set,sep=",",nrows=num_sample,header=None).values
test_hints = test_CSV[:][:,0]
test_sols = test_CSV[:][:,1]

# for noisy hints
logits = pickle.load(open("MNIST_test_marginal", "rb"))
logits_len = list(map(lambda x: len(x), logits))

num_nodes = 81
num_values = 9

m = np.array([1, num_nodes])    # number of nodes for each type of distribution (plus initial value 1)
dim = np.array([1, num_values]) # dimension for each type of distribution (plus initial value 1)
d = np.sum(np.multiply(m, dim))-1

# Load the precomputed A matrix
A = pickle.load(lzma.open(A_matrix_fn,"rb"))

with open("lambdas/lambda-0-"+sys.argv[1],'r') as f:
    lamb = float(f.read())
print(Fore.CYAN + "Lambda is",lamb)

Z_init = np.ones([d+1,d+1])*0.2
U_init = np.zeros([d+1, d+1])
ctime = time.process_time()
CFNdata = ADMM(A, Z_init, U_init, lamb, training_size, m, dim, norm_type, 4)
ADMM_time = time.process_time()-ctime

func_count,exact = CFNcount(*CFNdata)
print("The CFN has",func_count,"binary functions")
print("The CFN has only (soft) differences: ",exact,Style.RESET_ALL)
        
ndiff = 0
bad = 0
total_tb2_time = 0

for s,hint in enumerate(test_hints):
    ltruth = [int(v)-1 for v in test_sols[s].strip()]
    lhint = [int(v)-1 for v in hint]
    sol = []
    tb2_time = 0
    sol, tb2_time = find_best_sol0(CFNdata, lhint)
    total_tb2_time += tb2_time
    
    if (sol):
        pgrid(ltruth,lhint,list(sol[0]), None)
        # exact solution known, we can count the number of
        # wrong cells
        diff = sum(a != b for a, b in zip(list(sol[0]), ltruth))
        ndiff += diff
        if (diff > 0):
            print(Fore.RED,"Best solution has score:",diff," Sample",s+1,"/",num_sample,Style.RESET_ALL)
            bad += 1
        else:
            print(Fore.GREEN,"Zero score solution found. Sample",s+1,"/",num_sample,Style.RESET_ALL)
        # no solution found in the backtrack budget, this is bad, all cell predictions are bad too
    else:
        pgrid(ltruth,lhint,None, None)
        print(Fore.RED,"No solution found. Sample",s+1,"/",num_sample,Style.RESET_ALL)
        bad += 1
        ndiff += 81*(20 if mode == 2 else 1)

probe = (lamb, num_sample, bad, ndiff/(num_sample*81), ADMM_time, total_tb2_time, func_count, exact)

print(Fore.CYAN +"======================================")
print("=====> Lambda:", lamb)
print("=====> Number of incorrect solutions :", bad,"/", num_sample)
print("=====> Ratio of wrongly guessed cells:", probe[3])
print("=====> ADMM cpu-time                 :", probe[4])
print("=====> Toulbar2 average cpu-time     :", probe[5]/num_sample)
print("=====> Number of functions in model  :", probe[6])
print("=====> Exact model                   :", probe[7])
print("======================================", Style.RESET_ALL)



