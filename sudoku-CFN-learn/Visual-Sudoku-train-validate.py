import numpy as np
import math,time,sys
import lzma,os
from PEMRF import *
import pandas as pd
from colorama import Fore, Style
import pickle

if (len(sys.argv) != 3):
    print("Bad number of arguments!")
    print("training size [1000-180000] test set [satnet/rrn-??]")
    exit()

norm_type = "l1"
with open("lambdas/lambda-2-"+sys.argv[1],'r') as f:
    lamb = float(f.read())

training_size= int(sys.argv[1])
num_sample = 1000
A_matrix_fn = os.path.join("train-sufficient-statistics","B_"+str(training_size))+".xz"
test_set = sys.argv[2]

valid_CSV = pd.read_csv(test_set+".csv.xz",sep=",",nrows=num_sample, header=None).values
valid_hints = valid_CSV[:][:,0]
valid_sols = valid_CSV[:][:,1]

# for noisy hints
logits = pickle.load(open("MNIST_test_marginal", "rb" ))
logits_len = list(map(lambda x: len(x), logits))

def MNIST_transform(sample, sample_idx):
    lw = [[] for i in sample]
    for idx,c in enumerate(sample):
        if (c != '0'):
            img_idx = hash(c+str(idx)+str(sample_idx)) % logits_len[int(c)]
            lwi = list(map(lambda x: max(0,x), logits[int(c)][img_idx][1:]))
            minlwi = min(lwi)
            lw[idx] = list(map(lambda x: (x - minlwi),lwi))
    return lw


# decodes a grid (hint or solution) assuming it needs to be Lenet decoded
def MNIST_decode(sample, sample_idx):
    dec = []
    for idx,c in enumerate(sample):
        if (c != '0'):
            img_idx = hash(c+str(idx)+str(sample_idx)) % logits_len[int(c)]
            dec.append(logits[int(c)][img_idx][1:].argmin()+1)
        else:
            dec.append(0)
    return dec


# print the true solution with hints, its LeNet decoded variant (if given, mode
# 1/2) and the predicted solution (if given/found)
def pgrid(mode,lt,lh,lp=None,ld=None):
    print()
    print("   S O L U T I O N            ",end='')
    if (ld): print("  D E C O D E D             ",end='')
    if (lp): print("P R E D I C T E D",end='')
    print('\n')
    for i in range(9):
        for j in range(3):
            print(" ".join([Fore.WHITE+str(a+1) if a==b else Style.RESET_ALL+str(a+1) for a,b in zip(lt[i*9+j*3:i*9+j*3+3],lh[i*9+j*3:i*9+j*3+3])]),end='   ')
        print(end='    ')
        if (ld and mode == 2):
            for j in range(3):
                print(" ".join([Fore.GREEN+str(a) if (a-1)==b else Fore.RED+str(a) for a,b in zip(ld[i*9+j*3:i*9+j*3+3],lt[i*9+j*3:i*9+j*3+3])]),end='   ')
            print(end='    ')
        if (ld and mode == 1):
            for j in range(3):
                print(" ".join([Style.RESET_ALL+"-" if b<0 else Fore.GREEN+str(a) if (a-1)==b else Fore.RED+str(a) for a,b in zip(ld[i*9+j*3:i*9+j*3+3],lh[i*9+j*3:i*9+j*3+3])]),end='   ')
            print(end='    ')      
        if (lp):
            if (mode > 0):
                for j in range(3):
                    print(" ".join([Fore.GREEN+str(min(9,a+1)) if a==b else Fore.RED+str(min(9,a+1)) for a,b in zip(lp[i*9+j*3:i*9+j*3+3],lt[i*9+j*3:i*9+j*3+3])]),end='   ')
            else:
                for j in range(3):
                    print(" ".join([Fore.WHITE+str(b+1) if c==b else Fore.GREEN+str(min(9,a+1)) if a==b else Fore.RED+str(min(9,a+1)) for a,b,c in zip(lp[i*9+j*3:i*9+j*3+3],lt[i*9+j*3:i*9+j*3+3],lh[i*9+j*3:i*9+j*3+3])]),end='   ')                    
        print()
        if (i%3 == 2): print()
    print(Style.RESET_ALL)

num_nodes = 81
num_values = 9
m = np.array([1, num_nodes])    # number of nodes for each type of distribution (plus initial value 1)
dim = np.array([1, num_values]) # dimension for each type of distribution (plus initial value 1)
d = np.sum(np.multiply(m, dim))-1

# Load the precomputed A matrix
A = pickle.load(lzma.open(A_matrix_fn,"rb"))

print(Fore.CYAN + "Lambda is",lamb)

Z_init = np.ones([d+1,d+1])*0.2
U_init = np.zeros([d+1, d+1])
ctime = time.process_time()
CFNdata = ADMM(A, Z_init, U_init, lamb, training_size, m, dim, norm_type, 4)
ADMM_time = time.process_time()-ctime
        
func_count,exact = CFNcount(*CFNdata)
print("The CFN has",func_count,"binary functions")
print("The CFN has only (soft) differences: ",exact,Style.RESET_ALL)
        
ndiff = 0
bad = 0
tb2_time = 0

for s,hint in enumerate(valid_hints):
    lhint = [int(v)-1 for v in hint]
    cfn = toCFN(*CFNdata, weight = MNIST_transform(hint,s))
    cfn.UpdateUB(1e-6)
    ctime = time.process_time()
    sol = cfn.Solve()
    tb2_time += time.process_time()-ctime
    del cfn
    if (not sol):
        cfn = toCFN(*CFNdata, weight = MNIST_transform(hint,s))
        ctime = time.process_time()            
        sol = cfn.Solve()
        tb2_time += time.process_time()-ctime
        del cfn
            
    if (sol):
        ltruth = [int(v)-1 for v in valid_sols[s].strip()]
        pgrid(2,ltruth,lhint,list(sol[0]), MNIST_decode(valid_sols[s].strip(), s))
        diff = sum(a != b for a, b in zip(list(sol[0]), ltruth))
        ndiff += diff
        if (diff > 0):
            print(Fore.RED,sol[2],"solution(s). Nb wrong cells:",diff," for",s,"over",num_sample,Style.RESET_ALL)
            bad += 1
        else:
            print(Fore.GREEN,sol[2],"exact solution for",s,"over",num_sample,Style.RESET_ALL)
    else:
        print(Fore.RED, sol[2],"solution found for ",s,"over",num_sample,Style.RESET_ALL)
        bad += 1
        ndiff += 81
                
probe = (lamb, num_sample, bad, ndiff/(num_sample*81), ADMM_time, tb2_time, func_count, exact)

print(Fore.CYAN +"======================================")
print("=====> Lambda:", lamb)
print("=====> Number of incorrect solutions :", bad,"/", num_sample)
print("=====> Ratio of wrongly guessed cells:", probe[3])
print("=====> ADMM cpu-time                 :", probe[4])
print("=====> Toulbar2 average cpu-time     :", probe[5]/num_sample)
print("======================================", Style.RESET_ALL)


