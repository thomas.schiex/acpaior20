import CFN
import numpy as np
import math
import itertools
import pandas as pd

# Adds a clique of differences with violation "cost" on "varList"
def addCliqueAllDiff(theCFN, scope):
    different = (theCFN.GetUB()*np.identity(size, dtype=np.int64)).flatten()
    for vp in itertools.combinations(scope,2):
        theCFN.AddFunction(vp,different)

# Sets the value of variable with index "vIdx" to "value" using a unary function
def setHint(theCFN,vIdx,value):
    costs = myCFN.GetUB()*np.ones(size, dtype = np.int64)
    costs[value-1] = 0
    theCFN.AddFunction([vIdx], costs)

def printGrid(l):
    for i,v in enumerate(l):
        print((v if v else "-"), end=(' ' if (i+1)%size else '\n'))

################################################################

myCFN = CFN.CFN(1)  # This is the upper bound (k) to use

# Prefilled grids/solutions pairs from the SAT-Net paper (0 meaning unknown)
valid = pd.read_csv("satnet.csv.xz",sep=",", header=None).values
hints = valid[:][:,0]
sols  = valid[:][:,1]
size  = math.isqrt(len(sols[0])) # 9, for usual sodokus
param = math.isqrt(size)         # 3, the size of a subcells

grid = [int(h) for h in hints[0]]

# list of row, column and cells variable indices
rows = [ [] for _ in range(size) ]
columns = [ [] for _ in range(size) ]
cells = [ [] for _ in range(size) ]

# create variables and keep indices in row, columns and cells 
for i in range(size):
    for j in range(size):
        vIdx = myCFN.AddVariable("X"+str(i+1)+"."+str(j+1),range(1,size+1))
        columns[j].append(vIdx)
        rows[i].append(vIdx)
        cells[(i//param)*param+(j//param)].append(vIdx)

# add the clique constraints on rows, columns and cells
for scope in rows+columns+cells:
    addCliqueAllDiff(myCFN,scope)

# fill-in hints: a string of values, 0 denote empty cells
for v,h in enumerate(grid):
    if h:
        setHint(myCFN,v,h)
        
sol = myCFN.Solve() # if consistent, returns a triple with
                    # solution, cost, # of solutions found (counting)

printGrid(grid)
print("\n")
if (sol):
    printGrid(sol[0])
else:
    print("No solution")
