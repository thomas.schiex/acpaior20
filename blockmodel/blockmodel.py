import sys
import CFN
import networkx as nx
import matplotlib.pyplot as plt

Lines = open(sys.argv[1], 'r').readlines()
N = len(Lines)
Matrix = [[int(e) for e in l.split(' ')] for l in Lines]

K = int(sys.argv[2])

Var = [(chr(65 + i) if N < 28 else "x" + str(i)) for i in range(N)] # Political actor or any instance
#    Var = ["ron","tom","frank","boyd","tim","john","jeff","jay","sandy","jerry","darrin","ben","arnie"] # Transatlantic
#    Var = ["justin","harry","whit","brian","paul","ian","mike","jim","dan","ray","cliff","mason","roy"] # Sharpstone
#    Var  = ["Sherrif","CivilDef","Coroner","Attorney","HighwayP","ParksRes","GameFish","KansasDOT","ArmyCorps","ArmyReserve","CrableAmb","FrankCoAmb","LeeRescue","Shawney","BurlPolice","LyndPolice","RedCross","TopekaFD","CarbFD","TopekaRBW"] # Kansas

# create the input graph
G = nx.DiGraph()
for i in range(N):
    for j in range(N):
        if Matrix[i][j]:
            G.add_edge(Var[i], Var[j])

# make these lines a comment to skip it
nx.draw(G, with_labels = True)
plt.show(block = True) # if True then stop until plot window is closed

Top = 1 + N*N
Problem = CFN.CFN(Top)
CFN.tb2.option.showSolutions = 3
CFN.tb2.option.verbose = -1
CFN.tb2.check()

for u in range(K):
    for v in range(K):
        Problem.AddVariable("M_" + str(u) + "_" + str(v), range(2))

for i in range(N):
        Problem.AddVariable(Var[i], range(K))

for u in range(K):
    for v in range(K):
        for i in range(N):
            for j in range(N):
                if i != j:
                    Problem.AddFunction(["M_" + str(u) + "_" + str(v), Var[i], Var[j]],
                                        [1 if (u == k and v == l and Matrix[i][j] != m) else 0
                                         for m in range(2) for k in range(K) for l in range(K)])

# self-loops
#...

# breaking partial symmetries by fixing first (K-1) domain variables to be assigned to cluster less than or equal to their index
#...

Problem.Dump(sys.argv[1].replace('.mat','_' + str(K) + '.wcsp'))
res=Problem.Solve() # returns (solution vector, optimum, number of solutions found)
print("Optimum:", res[1], "in", Problem.GetNbBacktracks(), "backtracks and", Problem.GetNbNodes(), "nodes.")

