# Tutorial ACP-AI-OR-20: Block Modelling Problem

1. Open a terminal and run blockmodel.py on simple.mat with 3 blocks to visualize the input digraph and then the block structure and node clustering solution.
```
cd blockmodel
python3 blockmodel.py simple.mat 3 | awk -f ./sol2block.awk
```

2. Run Visual Studio Code. Open folder `blockmodel`. Open `blockmodel.py`. Modify the code to take into account self-loops (Hint: add binary cost functions on `M_u_u` and `Var[i]`).
```
python3 blockmodel.py transatlantic.mat 3 | awk -f ./sol2block.awk
```

3. Add partial symmetry breaking constraints on node variables (Hints: first var is assigned to block 0, second var to 0 or 1, third to 0, 1, or 2, etc.). Observe the time improvement.
```
time python3 blockmodel.py transatlantic.mat 3 | awk -f ./sol2block.awk
```

4. Check if the input graph is symmetric and avoid creating both `M_u_v` and `M_v_u` if so (Hint: do not create ternary cost functions if `u>v`).
```
time python3 blockmodel.py politicalactor.mat 5 | awk -f ./sol2block.awk
```

5. Order node variables by decreasing out degree and block variables by main diagonal first followed by `k`-diagonals with `k in 1..K-1`.
```
time python3 blockmodel.py transatlantic.mat 4 | awk -f ./sol2block.awk
```

6. Compare your results with the integer linear programming solver SCIP using both direct and support (aka `local polytope`) encodings.
```
python3 blockmodel.py sharpstone.mat 3 | awk -f ./sol2block.awk
python3 wcsp2lp-direct.py sharpstone_3.wcsp sharpstone_3_d.lp
scip -c 'read sharpstone_3_d.lp opt quit'
python3 wcsp2lp-support.py sharpstone_3.wcsp sharpstone_3_s.lp
scip -c 'read sharpstone_3_s.lp opt quit'
```

7. Compare with the MiniZincIDE constraint programming framework. Try `blockmodel.mzn` with different instances and number of blocks.
```
awk -f ./mat2dzn.awk sharpstone.mat > sharpstone.dzn
awk -f ./mat2dzn.awk politicalactor.mat > politicalactor.dzn
awk -f ./mat2dzn.awk transatlantic.mat > transatlantic.dzn
awk -f ./mat2dzn.awk kansas.mat > kansas.dzn
awk -f ./mat2dzn.awk 30.mat > 30.dzn
awk -f ./mat2dzn.awk 100.mat> 100.dzn
awk -f ./mat2dzn.awk 200.mat> 200.dz
minizinc blockmodel.mzn sharpstone.dzn
```

You can find instances in compressed CFN format [here](http://genoweb.toulouse.inra.fr/~degivry/evalgm/CFN/BlockModeling) (without sorting variables nor detecting symmetric matrix) and [there](http://genoweb.toulouse.inra.fr/~degivry/evalgm/CFN/BlockModeling2) (both sorting variables and exploiting symmetric matrices). Data files and corrected models are also available [here](https://forgemia.inra.fr/thomas.schiex/cost-function-library/-/tree/master/crafted/blockmodel).

